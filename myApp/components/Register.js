import React from 'react';
import { StyleSheet, Text, View, Image ,SafeAreaView, TextInput} from 'react-native';

export default function Register() {

    const [fullname, onChangeText] = React.useState(null);
    const [email, onChangeNumber] = React.useState(null);
    const [password, onChangePassword] = React.useState(null);
  return (
    <View style={styles.canvas}>
      <View style={styles.container}>
        <View style={styles.header}>
          <Text style={styles.title}>ChatKuy </Text>
          <Image style={styles.Logo} source={require('../assets/logo.png')} />
        </View>
        <Text style={styles.title}>Register </Text>
        <View>
          <SafeAreaView>
          <Text style={styles.from}>FULL NAME </Text>
            <TextInput style={styles.input}
              onChangeText={onChangeText}
              value={fullname}
            />
            <Text style={styles.from}>EMAIL </Text>
            <TextInput style={styles.input}
              onChangeText={onChangeNumber}
              value={email}
              keyboardType="numeric"
            />
            <Text style={styles.from}>PASSWORD </Text>
            <TextInput style={styles.input}
              onChangeText={onChangePassword}
              value={password}
              keyboardType="numeric"
            />
          </SafeAreaView>
          <View style={styles.register}>
            <Text style={styles.textRegister}>
              Register
            </Text>
          </View>
          <View style={styles.login}>
            <Text style={styles.textLogin}>
              Login
            </Text>
          </View>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  canvas: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'black',
  },
  container: {
    width: 408,
    height: 893,
    backgroundColor: 'white',
    alignItems: 'center',
  },
  header: {
    width: 408,
    height: 285,
    backgroundColor: '#1E7BF6',
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 70,

  },
  Logo: {
    position: 'absolute',
    top: 235,
    left: 145,
    width: 114,
    height: 107,
    borderRadius: '50%'
  },
  title: {
    fontSize: 32,
  },
  input:{
    paddingLeft:25,
    marginTop:5,
    marginBottom:20,
    width: 342,
    height:68,
    borderWidth:1,
  },
  login:{
    width: 342,
    height:68,
    backgroundColor:'white',
    justifyContent:'center',
    alignItems:'center',
    marginBottom:10,
    borderWidth:1,
    borderColor:'#1E7BF6',
  },
  register:{
    width: 342,
    height:68,
    backgroundColor:'#1E7BF6',
    justifyContent:'center',
    alignItems:'center',
    marginBottom:10
  },
  textRegister:{
    fontSize:17,
    color:'white',
  },
  textLogin:{
    fontSize:17,
    color:'#1E7BF6',
  }
});