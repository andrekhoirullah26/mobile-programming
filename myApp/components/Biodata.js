import React from 'react';
import { StyleSheet, Text, View, Image ,SafeAreaView, TextInput} from 'react-native';

export default function Biodata() {
return (
    <View style={styles.canvas}>
    <View style={styles.container}>
        <View style={styles.header}>
            <Image style={styles.logo} source={require('../assets/latar.png')}/>
            <Image style={styles.Logotitle} source={require('../assets/logo.png')} />
            <Image style={styles.prof} source={require('../assets/profil.jpg')}/>
            <Text style={styles.title}>chatkuy </Text>
        </View>
        <Text style={styles.nama}>Andre Khoirullah </Text>
        <Text style={styles.text}>181351016 </Text>
        <Text style={styles.text}>Teknik Informatika </Text>
        <Text style={styles.kelas}>Pagi B </Text>
        <View style={styles.body}> 
        
        <Image style={styles.icon} source={require('../assets/tgl.png')}/>
        <Image style={styles.icon} source={require('../assets/imh.png')}/>
        <Image style={styles.icon} source={require('../assets/ig.png')}/>

        <Text style={styles.tgl}>26 Nopember 1999</Text>
        <Text style={styles.alamat}>Cipeundeuy, Kec.Cipeundeuy, Kab.Subang </Text>
        <Text style={styles.ig}>andre_khoirullah26 </Text>
        </View>

    </View>
    </View>
    );
}

const styles = StyleSheet.create({
    canvas: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'black',
    },
    container: {
        width: 408,
        height: 893,
        backgroundColor: 'white',
        alignItems: 'center',
    },
    header: {
        width: 408,
        height: 285,
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 70,
    },
    logo:{
        width: 408,
        height: 285,
    },
    prof: {
        position: 'absolute',
        top: 150,
        left: 145,
        width: 150,
        height: 150,
        borderRadius: '50%'
    },
    text: {
        fontSize:25,

    },
    nama: {
        fontSize:36,
    },
    kelas: {
        fontSize:18,
    },
    icon: {
        width:47,
        height:55,
        alignItems: 'flex-end',
        justifyContent: 'flex-start',
        margin:10,
        
    },
    body:{
        width:400,
        marginTop:100,
        position:'absolute',
        top:500,
        left:50,
    },
    alamat:{
        paddingLeft:70,
        marginTop:100,
        position:'absolute',
        top:5,
        fontSize:15,

    },
    tgl:{
        paddingLeft:70,
        marginTop:100,
        position:'absolute',
        top:-70,
        fontSize:15,
    
    },
    ig:{
        paddingLeft:70,
        marginTop:100,
        position:'absolute',
        top:70,
        fontSize:15,
        
    },
    title:{
        position:'absolute',
        left:50,
        top:70,
        fontSize:20,
        fontWeight:'bold'
    },
    Logotitle:{
        width:50,
        height:50,
        position:'absolute',
        top:65,
        left:140,
        borderRadius:50
    }




    })