class Animal {
    constructor(name) {
        this.sapi = name ;
        this.kancil = 4 ;
        this.indonesia = false;
} get name() {
    return this.sapi;
} get legs() {
    return this.kancil;
} get cold_blooded() {
    return this.indonesia;
} set name(x) {
    this.sapi = x;
} set legs(y) {
    this.kancil = y;
} set cold_blooded(z) {
    this.indonesia = false;
}
}
class Ape extends Animal {
    constructor(name) {
        super(name)
        this.sapi = name;
        this.kancil = 2;
    }
    yell() { return console.log("Auooo"); }
}

class Frog extends Animal {
    constructor(name) {
        super(name)
        this.sapi = name;
    }
    jump() { return console.log("hop hop"); }
}
    
    

let sheep = new Animal("shaun");
console.log(sheep.sapi) // "shaun"
console.log(sheep.kancil) // 4
console.log(sheep.indonesia) // false
console.log("");

let sungokong = new Ape("kera sakti")
console.log(sungokong.sapi); //"kera sakti"
console.log(sungokong.kancil); //2
console.log(sungokong.indonesia) // false
sungokong.yell() // "Auooo"
console.log("");

let kodok = new Frog("buduk")
console.log(kodok.sapi); //buduk
console.log(kodok.kancil); //4
console.log(kodok.indonesia) // false
kodok.jump() // "hop hop"
console.log("");

//02
class Clock {
    constructor({ template }) {
        this.template = template;
    }

    render() {
        let date = new Date();

        let hours = date.getHours();
        if (hours < 10) hours = '0' + hours;

        let mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;

        let secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;

        let output = this.template
            .replace('h', hours)
            .replace('m', mins)
            .replace('s', secs);

        console.log(output);
    }

    stop() {
        clearInterval(this.timer);
    }

    start() {
        this.render();
        this.timer = setInterval(() => this.render(), 1000);
    }
}
var clock = new Clock({ template: 'h:m:s' });
clock.start();